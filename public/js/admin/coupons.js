// Define a new component called button-counter
Vue.component('Blogging', {
    props: ['title'],
    template: '<h3>{{ title }}</h3>'
  })
var pp1 = new Vue({ 
    el: '#components-1',
    data : {
        posts: [
            { id: 1, title: 'My journey with Vue' },
            { id: 2, title: 'Blogging with Vue' },
            { id: 3, title: 'Why Vue is so fun' }
        ]
    }
});
var pp2 = new Vue({ 
    el: '#components-2',
    data : {
        posts: [
            { id: 1, title: 'My journey with Vue' },
            { id: 2, title: 'Blogging with Vue' }
        ]
    }
});