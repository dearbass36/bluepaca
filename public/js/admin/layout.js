$(function(){
    var loc = window.location, new_uri;
    if (loc.protocol === "https:") {
        new_uri = "wss:";
    } else {
        new_uri = "ws:";
    }
    new_uri += "//" + 'localhost:8080';
const socket = new WebSocket(new_uri);
var pathArray = window.location.pathname.split('/');
socket.onopen = function(e) {
    console.log("Connection established!");
}
    switch(pathArray[2]) {
        case "order":
            $("#order_menu").addClass('active');
            break;
        case "product":
            $("#product_menu").addClass('active');
            break;
        case "promotion":
            $("#promotion_menu").addClass('active');
            break;
        case "coupons":
            $("#coupons_menu").addClass('active');
            break;
        case "flashsale":
            $("#flashsale_menu").addClass('active');
            break;
        default:
            $("#home_menu").addClass('active');
    }
			socket.onmessage = function(e){
                switch(pathArray[2]) {
                    case "order":
                        refresh_table()
				        refresh_notic()
                        break;
                    default:
                        refresh_notic()
                }
				
			}

});
