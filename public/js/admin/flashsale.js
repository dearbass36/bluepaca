var choose_p_modal = new Vue({
    el : "#choose_p_modal",
    data : {
        rowdata : [],
        selected_Product:[],
        search : ''
    },
    computed : {
        filteredList() {
            return this.rowdata.filter((post) => {
                var alltext = post.p_brand_EN.toLowerCase()+post.p_name_TH.toLowerCase()
              return alltext.includes(this.search.toLowerCase())
            })
        },
        LoadData_row : function (){ 
            var el = this
            axios.get('/get_allproduct')
                .then(function (response) {
                    var data = response.data;
                        for(var i in data){
                            el.rowdata.push(data[i])
                        }
            }); 
        }
    },
    watch : {
        
    },
    methods : {
        Save_choose : function (){
            var el = this;
            var selected = el.selected_Product;
            for(var i in selected){
                var result = el.rowdata.find((q) => {
                    return q.p_id == selected[i];
                });
                var n = el.rowdata.indexOf(result);
                new_fs.row.push(result);
                new_fs.discount.push({mode : 1 , discount : 0});
                el.rowdata.splice(n,1);
            }
            el.selected_Product = [];
        },
        Select_pro : function (p_id){
            var el = this;
            var select = el.selected_Product;
            var boo = select.includes(p_id);
            if(boo == true){
                select.splice(select.indexOf(p_id),1);
            }
            else{
                select.push(p_id);
            }
        }
    }
});
var now_fs = new Vue({
    el : '#now_fs',
    data : {
        datenow : '',
        fsnow : [],
        displayTime : {
            hour : '00',
            min : '00',
            sec : '00' 
        },
        fs_show : []

    },
    computed : {
        
    },
    methods : {
        time : function(){
            var el = this;
            setInterval(() => {
                el.datenow = moment().format('YYYY-MM-DD HH:mm:ss');
                let now,end,duration,sec,min,hour,objtime               
                now = moment(el.datenow);
                end = moment(el.fsnow.fs_dateend);
                duration = moment.duration(end.diff(now));
                sec = lessthanTen(duration.seconds());
                min = lessthanTen(duration.minutes());
                hour = lessthanTen(duration.hours());
                objtime = {hour : hour , min : min , sec : sec };
                el.displayTime = objtime;
            }, 1000);
            
        },
        cal_price :  function(x,y,z){
            if(y == 1){
                return x-((x*z)/100).toFixed(0);
            }
            else{
                return x-z.toFixed(0);
            }
        },
        getnowFS : async function(){
            let el = this;
            const data = await axios.get('/testfs');
            if(data.data.length > 0){
                this.fsnow = data.data[0];
                let detailrow = new_fs.detailrow;
                console.log(detailrow);
                    let result = detailrow.filter((q)=>{
                                    return q.fs_key == this.fsnow.fs_key
                                });
                    console.log("Y");
                    this.fs_show = result;
            }
            else{
                el.fs_show = [];
                el.fsnow = [];
                el.displayTime.hour = '00',
                el.displayTime.min = '00',
                el.displayTime.sec = '00' 
            }
        }
    },
    mounted: function() {
        this.time();
        
      },
    beforeMount(){
        
    }
});
var new_fs = new Vue({
    el : "#new_fs",
    data : {
        row : [],
        discount : [],
        tablerow : [],
        opened : [],
        detailrow : []
    },
    computed : {
        LoadData_table : function(){
            var el = this;
            var x = 
            new Promise((resolve, reject) => { 
                axios.get('/get_allfs')
              .then(function (response) {
                  var data = response.data;
                        for(var i in data){
                            el.tablerow.push(data[i])
                        }
                        resolve(el.tablerow);
              })
            });
            var y = new Promise((resolve, reject) => {
                    axios.get('/find_fskey')
                    .then(function(response){
                        var data = response.data;
                            for(var i in data){
                                el.detailrow.push(data[i])
                            }
                    resolve(el.detailrow);
                })
            });

                Promise.all([x, y]).then(function(values) {
                    now_fs.getnowFS();
                  }); 
        }
    },
    watch : {

    },
    methods : {
        Del_choose : function (index){
            let el = this;
            let data = el.row[index];
            choose_p_modal.rowdata.push(data);
            choose_p_modal.rowdata.sort(compare);
            el.row.splice(index,1);
            el.discount.splice(index,1)
        },
        Create_click : function(){
            var el = this;
            var general = {
                fs_des : el.$ref.fs_des.value,
                fs_date : el.$ref.fs_date.value,
                fs_start : el.$ref.fs_start.value,
                fs_end : el.$ref.fs_end.value,
            }
            axios.post('/save_flashsale',{
                general : general,
                row : new_fs.row,
                discount : new_fs.discount
            })
                .then(function(response){
                    axios.get('/find_fskey')
                        .then(function(response){
                            el.detailrow = response.data;
                    }); 
                    el.tablerow.push(
                                        {
                                            fs_dateend : general.fs_date+" "+general.fs_end+":00",
                                            fs_datestart : general.fs_date+" "+general.fs_start+":00",
                                            fs_description : general.fs_des,
                                            fs_key : response.data.key,
                                            fs_status : 0
                                        }    
                                    );
                    swal({
                        title: 'Success',
                        text: 'เพิ่ม Flash Sale เรียบร้อย !!',
                        type: 'success'
                    })
                    clear_form();
                });
        },
        Cancel_click : function(){
            clear_form();
        },
        Del_fs : function (index){
            var el = this;
            var key = el.tablerow[index].fs_key;
            swal({
                title: 'คุณแน่ใจที่จะลบ?',
                text: "หากลบแล้วไม่สามารถกู้ข้อมูลกลับมาได้",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ใช่,ฉันจะลบ',
                cancelButtonText: 'ยกเลิก'
              }).then((result) => {
                    if (result.value) {
                        axios.delete('/delete_fs/'+key)
                                                        .then(function(response){
                                                                el.tablerow.splice(index,1);
                                                                swal({
                                                                    title: 'Deleted !',
                                                                    text: 'ลบ Flash Sale เรียบร้อย !!',
                                                                    type: 'error'
                                                                });
                                                                now_fs.getnowFS();
                                                        });
                    }
                    else {
                        
                    }
                })
                
        },
        Switch_status : function(index){
            var el = this;
            var key = el.tablerow[index].fs_key;
            var status = el.tablerow[index].fs_status;
                    el.tablerow.forEach((q)=>{
                        q.fs_key == key ?  q.fs_status == 1 : q.fs_status = 0;
                    });
            axios.get('/update_status_fs',{
                params : {
                    key : key,
                    status : status
                }
            })
                .then(function(response){
                    now_fs.getnowFS();
                });
                
                
        },
        Seedetail : function(key){
            var el = this;
            const index = el.opened.indexOf(key);
            if (index > -1) {
                el.opened.splice(index, 1)
            } else {
                el.opened.push(key)
            }
        }
    },
    beforeMount(){
        this.LoadData_table;
        choose_p_modal.LoadData_row;
       
    }
});

function clear_form(){
    var fs_row = new_fs.row;
    for(var i in fs_row){
        choose_p_modal.rowdata.push(fs_row[i]);
    }
    choose_p_modal.rowdata.sort(compare);
    new_fs.row = [];
    new_fs.discount = [];
    new_fs.$refs.fs_des.value = '';
    new_fs.$refs.fs_date.value = '';
    new_fs.$refs.fs_start.value = '';
    new_fs.$refs.fs_end.value = '';
}
function compare(a, b) {
    const genreA = a.p_id;
    const genreB = b.p_id;
    let comparison = 0;
    if (genreA > genreB) {
      comparison = 1;
    } else if (genreA < genreB) {
      comparison = -1;
    }
    return comparison;
}

function lessthanTen(x){
    parseInt(x) < 10 ? x = "0"+x : x = x ;
    return x;
}

