$(function(){
    
    $(".close").click(function (){
        var Parent = $(this).parents('.modal-img').hide();
    })
    $(".modal-img").click(function(event){
        $(this).hide();
    });
    $(".img-mo").click(function(event){
        event.stopPropagation();
    });
});

function refresh_table(){
    $.get('/get_allorder',function(response){
        var text = "" ;
        for(var i in response){
            text += `<tr class="odd gradeX" data-id="${response[i].order_id}">
                        <td width="1%" class="f-s-600 text-inverse">${(parseInt(i)+1)}.</td>
                        <td width="1%" class="with-img"><a onclick="imgbtn_click(${response[i].order_id})"><img src="/picture/${response[i].order_img}" class="img-rounded height-80" /></a></td>
                        <td><a onclick="code_click(${response[i].order_id})"></a>${response[i].order_code}</td>
                        <td>${response[i].name}</td>
                        <td>${response[i].order_price}</td>
                        <td>${response[i].order_date}</td>
                        <td>${response[i].order_status}</td>
                    </tr>` ;
        }
        $("#t_body").html(text);
    });
}
function imgbtn_click(id){
    var parent = $('[data-id='+id+']').find('img');
    var img = parent[0].src;
    $(".img-mo").attr('src',img)
    $(".modal-img").show();
}
function code_click(order_id){
    location.assign("/posts?id="+order_id);
}