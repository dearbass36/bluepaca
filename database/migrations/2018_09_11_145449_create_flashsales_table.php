<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlashsalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flashsales', function (Blueprint $table) {
            $table->increments('fs_id');
            $table->string('fs_key');
            $table->string('fs_description');
            $table->integer('fs_mode');
            $table->integer('fs_discount');
            $table->integer('p_id');
            $table->dateTime('fs_datestart');
            $table->dateTime('fs_dateend');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flashsales');
    }
}
