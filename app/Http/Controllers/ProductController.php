<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;
use App\Product;
use App\sub_img;
use App\Category;
use App\Promotion;
use DB;

class ProductController extends Controller
{
    //
    public function add_product(Request $request){
        $message = [
            'brand_TH.required' => 'กรุณาใส่เเบรนด์ภาษาไทย',
            'name_TH.required' => 'กรุณาใส่ชื่อผลิตภัณฑ์ภาษาไทย',
            'price.required' => 'กรุณาใส่ราคาสินค้า',
            'price.numeric' => 'กรุณาใส่ตัวเลขในราคาสินค้า',
            'code.required' => 'กรุณาใส่รหัสสินค้า',
            'code.unique' => 'รหัสสินค้าถูกใช้ไปแล้ว' ,
            'code.numeric' => 'กรุณาใส่สินค้าเป็นตัวเลข',
            'level1.numeric' => 'กรุณาเลือกหมวดหมู่'
        ];
        $validator = Validator::make($request->all(), [
            'name_TH' => 'required',
            'brand_TH' => 'required',
            'price' => 'required|numeric',
            'code' => 'required|unique:products,p_code|numeric',
            'level1' => 'numeric'
        ],$message);
           
    
          if ($validator->passes()) {
            $input = $request->all();
            $product = new Product;
            $product->p_code = $input['code'];
            $product->p_brand_TH = $input['brand_TH'];
            $product->p_brand_EN = $input['brand_EN'];
            $product->p_name_TH = $input['name_TH'];
            $product->p_name_EN = $input['name_EN'];
            $product->p_detail_TH = $input['prop_TH'];
            $product->p_detail_EN = $input['prop_EN'];
            $product->p_img = $input['main_img'];
            $product->p_price = $input['price'];
            $product->p_discount = 0;
            $product->p_status = "สินค้าปกติ";
            $product->p_url = $input['url'];
            $product->p_keyword = $input['keyword'];
            $product->p_description = $input['descript'];
            $product->p_amount = 0;
            $c_id;
            if($input['level3'] != null ){
                $c_id = $input['level3'];
            }
            else {
                if($input['level2'] != null){
                    $c_id = $input['level2'];
                }
                else{
                    $c_id = $input['level1'];
                }
            }
            $product->c_id = $c_id;
            $product->save();
            if($input['sub_img'] != null){
                $p_id = $product->id;
                $img = explode(",",$input['sub_img']);
                foreach($img as $value){
                                    $sub_img = new sub_img; 
                                    $sub_img->p_id = $p_id;
                                    $sub_img->subimg_name = $value;
                                    $sub_img->save();
                }
            }
            return response()->json(['subid'=>$c_id]);
          }
          return response()->json(['error'=>$validator->errors()->all()]);
    }
    public function multiupload(Request $request){
    
        $image = $request->file('file');
        $ex = explode(".",$image->getClientOriginalName());
        $imageName = time().base64_encode($ex[0]).".jpg";
        $upload_success = $image->move(public_path('images'),$imageName);
      
            return $imageName;
      
    }
    public function delete_photo(Request $request){
        $filename = $request->id;
        $file = public_path('images')."/".$filename;
        unlink($file);
        return "Success";
    }
    public function upload_mainphoto(Request $request){
        $image = $request->file('main_file');
        $ex = explode(".",$image->getClientOriginalName());
        $imageName = time().base64_encode($ex[0]).".jpg";
        $upload_success = $image->move(public_path('picture'),$imageName);
      
        return $imageName;
    }
    public function edit_mainproduct(Request $request){
        $input = $request->query();
        Product::where('p_id',$input['id'])->update([
            'p_brand_TH' => $input['brand_TH'] ,
            'p_brand_EN' => $input['brand_EN'] ,
            'p_name_TH' => $input['name_TH'] ,
            'p_name_EN' => $input['name_EN'] ,
            'p_price' => $input['price'] ,
            'p_amount' => $input['amount']]);
        return response()->json(['success'=>'done']);
    }
    public function edit_prop(Request $request){
        $input = $request->query();
        Product::where("p_id",$input['id'])->update([
                'p_detail_TH'=>$input['prop_TH'],
                'p_detail_EN'=>$input['prop_EN']]);
        return response()->json(['success'=>'done']);
    }
    public function editImgUpload(Request $request){
        $id = $request->input('id_modalimg');
        $remove = $request->input('id_removeimg');
        $uploaded_main = "";
        if($remove == null){
        }
        else{
            $ff = explode(",",$remove);
            foreach($ff as $value){
                $img = sub_img::where('subimg_id',$value)->get();
                print_r($img[0]->subimg_name);
                $file = public_path('images')."/".$img[0]->subimg_name;
                unlink($file);
                sub_img::where('subimg_id', $value)->delete();
            }
        }
        $mainimg = $request->file('main_file');
            if($mainimg == null){
            }
            else{
                $ex = explode(".",$mainimg->getClientOriginalName());
                $imageName = time().base64_encode($ex[0]).".jpg";
                $uploaded_main = $imageName;
                $upload_success = $mainimg->move(public_path('picture'),$imageName);
                Product::where('p_id',$id)->update(['p_img'=>$imageName]);
            }
        $subimg = $request->file('subimg');
            if($subimg == null){
                
            }
            else{
                foreach($subimg as $value){
                $ex = explode(".",$value->getClientOriginalName());
                $imageName = time().base64_encode($ex[0]).".jpg";
                $upload_success = $value->move(public_path('images'),$imageName);
                $sub_img = new sub_img;
                $sub_img->p_id = $id;
                $sub_img->subimg_name = $imageName;
                $sub_img->save();
                }
            } 
        
        return response()->json(['img'=>$uploaded_main,'id'=>$id]);
    }
    public function dynamiccategory(Request $request){
        $value = $request->get('value');
        $data = Category::where('c_subid', $value)->get();
        $output = '<option value="">'.'- LEVEL'.($request->get('level')+1).' -'.'</option>';
        foreach($data as $row){
            $output .= '<option value="'.$row->c_id.'">'.$row->c_name_TH.'</option>';
        }
        echo $output;
    }
    public function editOther(Request $request){
        $c_id;
        if($request->query('level3') != null){
            $c_id=$request->query('level3');
        }
        else if($request->query('level3') == null && $request->query('level2') != null){
            $c_id=$request->query('level2');
        }
        else{
            $c_id=$request->query('level1');
        }
        $product = Product::where('p_id',$request->get('id'))->update(['p_url' =>$request->get('url')
                                                                ,'p_keyword' => $request->get('key')
                                                                ,'p_description' => $request->get('descript')
                                                                ,'p_status' => $request->get('status')
                                                                ,'c_id' =>$c_id]);
        return response()->json(['success'=>'done']);
    }
    public function category_edit(Request $request){
        $c_id = $request->get('c_id');
        $Find_Category = Category::where('c_id',$c_id)->get();
        $level = $Find_Category[0]->c_level;
        $output = array();
        $id_selected = array();
        $id_level1;
        $id_level2;
        $id_level3;
        $output_lv3 = '<option value="">- LEVEL3 -</option>';
        $output_lv2 = '<option value="">- LEVEL2 -</option>';
        $output_lv1 = '<option value="">- LEVEL1 -</option>';
        if($level == 3){
            $id_level3 = $c_id;
            $all_lv3 = Category::where('c_subid',$Find_Category[0]->c_subid)->get();
            foreach($all_lv3 as $row){
                $output_lv3 .= '<option value="'.$row->c_id.'">'.$row->c_name_TH.'</option>';
            }
            $id_level2 = $Find_Category[0]->c_subid;
            $Find_Cate2 = Category::where('c_id',$Find_Category[0]->c_subid)->get();
            $all_lv2 = Category::where('c_subid',$Find_Cate2[0]->c_subid)->get();
            foreach($all_lv2 as $row){
                $output_lv2 .= '<option value="'.$row->c_id.'">'.$row->c_name_TH.'</option>';
            }
            $id_level1 = $Find_Cate2[0]->c_subid;
        }
        else if($level == 2){
            $id_level3 = null ;
            $id_level2 = $c_id;
            $all_lv2 = Category::where('c_subid',$Find_Category[0]->c_subid)->get();
            foreach($all_lv2 as $row){
            $output_lv2 .= '<option value="'.$row->c_id.'">'.$row->c_name_TH.'</option>';
            }
            $id_level1 = $Find_Category[0]->c_subid;
        }
        else {
            $id_level1 = $c_id;
            $id_level2 = null;
            $id_level3 = null;
        }
        $all_lv1 = Category::where('c_level',1)->get();
        foreach($all_lv1 as $row){
            $output_lv1 .= '<option value="'.$row->c_id.'">'.$row->c_name_TH.'</option>';
        }
        array_push($output,$output_lv1);
        array_push($output,$output_lv2);
        array_push($output,$output_lv3);
        array_push($id_selected,$id_level1);
        array_push($id_selected,$id_level2);
        array_push($id_selected,$id_level3);
        return [$output,$id_selected];
    }
    public function save_duplicate(Request $request){
        $id = $request->get('id');
        $code = $request->get('code');
        $check_code = Product::where('p_code',$code)->get();
        if(!isset($check_code[0]->p_id)){
            $product_select = Product::where('p_id',$id)->get();
            $sub_img_select = sub_img::where('p_id',$id)->get();
            $product = new Product;
            $product->p_code = $code;
            $product->p_brand_TH = $product_select[0]->p_brand_TH;
            $product->p_brand_EN = $product_select[0]->p_brand_EN;
            $product->p_name_TH = $product_select[0]->p_name_TH;
            $product->p_name_EN = $product_select[0]->p_name_EN;
            $product->p_detail_TH = $product_select[0]->p_detail_TH;
            $product->p_detail_EN = $product_select[0]->p_detail_EN;
            $product->p_price = $product_select[0]->p_price;
            $product->p_discount = 0;
            $product->p_status = $product_select[0]->p_status;
            $product->p_url = $product_select[0]->p_url;
            $product->p_keyword = $product_select[0]->p_keyword;
            $product->p_description = $product_select[0]->p_description;
            $product->p_amount = 0;
            $product->c_id = $product_select[0]->c_id;

            $filename = time().rand().'.jpg';
            $file = public_path('picture').'/'.$product_select[0]->p_img;
            $newfile = public_path('picture')."/".$filename;
            if (!copy($file, $newfile)) {
                return response()->json(['success'=>'fail to copy']);
            }
            $product->p_img = $filename;
            $product->save();
            foreach($sub_img_select as $row){
                $filename = time().rand().'.jpg';
                $file = public_path('images').'/'.$row->subimg_name;
                $newfile = public_path('images')."/".$filename;
                $copy = copy($file, $newfile);
                $data = new sub_img;
                $data->p_id = $product->id;
                $data->subimg_name = $filename;
                $data->save();
            }
            return $product;
        }
        else{
            return response()->json(['success'=>'fail to copy']);
        }
    }
    public function save_discount(Request $request){
        $data = $request->query();
        $id = $data['id'];
        $discount = $data['discount'];
        $date_start = $data['date_start'];
        $date_end = $data['date_end'];
        Product::where('p_id',$id)->update(['p_discount' => $discount]);
        $check_pro = Promotion::where('p_id',$id)->get();
        if(count($check_pro) == 0){
            $promotion = new Promotion; 
            $promotion->p_id = $id;
            $promotion->pro_datestart = $date_start;
            $promotion->pro_dateend = $date_end;
            $promotion->p_discount = $discount;
            $promotion->timestamps = false;
            $promotion->save();
        }
        else{
            $promotion = DB::table('promotions')->where('p_id',$id)->update([ 'p_discount' => $discount , 
                                                                'pro_datestart' => $date_start , 
                                                                'pro_dateend' => $date_end ]);
        }
        if( isset($promotion->id) || $promotion == 1){
            return response()->json(['success' => 'done']);
        }
        else {
            return response()->json(['success' => 'fail']);
        }
    }
}
