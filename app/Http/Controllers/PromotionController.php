<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;
use Hidehalo\Nanoid\Client;
use Hidehalo\Nanoid\GeneratorInterface;
use App\Product;
use App\sub_img;
use App\Category;
use App\Promotion;
use App\Coupons;
use App\Flashsale;
use DB;

class PromotionController extends Controller
{
    //
    public function save_coupons(Request $request){
        $data = $request->query();
        $message = [
            'cp_discount.numeric' => 'กรุณาใส่ตัวเลข',
            'cp_code.unique' => 'โค้ดนี้ถูกใช้ไปแล้ว',
            'cp_code.required' => 'กรุณาใส่โค้ด',
            'cp_discount.required' => 'กรุณาใส่ส่วนลด',
            'cp_datestart.required' => 'กรุณาใส่วันเริ่มต้น',
            'cp_dateend.required' => 'กรุณาใส่วันหมดอายุ'
        ];
        $validator = Validator::make($request->all(), [
            'cp_discount' => 'required|numeric',
            'cp_code' => 'required|unique:coupons,cp_code',
            'cp_datestart' => 'required',
            'cp_dateend' => 'required'
        ],$message);

        if ($validator->passes()) {
            $Coupons = new Coupons;
            $Coupons->cp_code = $data['cp_code'];
            $Coupons->cp_mode = $data['cp_mode'];
            $Coupons->cp_discount = $data['cp_discount'];
            $Coupons->cp_datestart = $data['cp_datestart'];
            $Coupons->cp_dateend = $data['cp_dateend'];
            $Coupons->save();
            return response()->json(['success'=> 'done']);
        }
        else {
            return response()->json(['err'=>$validator->errors()->all()]);
        }
    }
    public function save_flashsale(Request $request){
        $general = $request->input('general');
        $row = $request->input('row');
        $discount = $request->input('discount');
        $count = count($row);
        $client = new Client();
        $key = $client->generateId($size = 9, $mode = Client::MODE_DYNAMIC);
        for($i=0;$i<$count;$i++){
            $fs = new Flashsale();
            $fs->fs_key = $key;
            $fs->fs_description = $general['fs_des'];
            $fs->fs_mode = $discount[$i]['mode'];
            $fs->fs_discount = $discount[$i]['discount'];
            $fs->p_id = $row[$i]['p_id'];
            $fs->fs_datestart = $general['fs_date']." ".$general['fs_start'];
            $fs->fs_dateend = $general['fs_date']." ".$general['fs_end'];
            $fs->fs_status = 0;
            $fs->save();
        }
        return response()->json(["succes" => "done" , "key" => $key]);
    }
}
