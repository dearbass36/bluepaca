<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hidehalo\Nanoid\Client;
use Hidehalo\Nanoid\GeneratorInterface;
use Carbon\Carbon;
use DB;
use Validator;
use App\Product;
use App\Order;
use App\Orderdetail;
use App\notification;
class OrderController extends Controller
{
    //
    public function save_order(Request $request){
        $order_code = $this->getnanoid();
        $totalprice = $request->get('totalprice');
        $data = $request->get('param');
        $owner = $request->get('owner');
        $id_all = "";
        $i = 0;
        foreach($data as $row){
            if($i == 0){
                $id_all = $row['p_id'];
            }
            else{
                $id_all = $id_all.",".$row['p_id'];
            }
            $i++;
        }
        $Order = new Order;
        $Order->order_code = $order_code;
        $Order->owner_id = $owner;
        $Order->order_img = "ordernow.png";
        $Order->order_price = $totalprice;
        $Order->order_status = "รอชำระเงิน";
        $Order->p_code = $id_all;
        $Order->save();

        $notic = new notification;
        $notic->order_id = $Order->id;
        $notic->notic_detail = "ได้สั่งสินค้ารหัส Order : ".$order_code." สถานะ : รอชำระเงิน";
        $notic->notic_status = 0;
        $notic->notic_mode = 1;
        $notic->save();
        $idOrder = $Order->id;
        foreach($data as $row2){
            Orderdetail::where('od_id',$row2['od_id'])->update(['od_status'=>1,'order_id'=>$idOrder]);
        }
        return response()->json(['success' => 'done']);
    }
    public function getnanoid(){
        $client = new Client();
        return $client->generateId($size = 21, $mode = Client::MODE_DYNAMIC);
    }
}
