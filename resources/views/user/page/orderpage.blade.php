@extends('user.layouts.layout')
@section('content')
<div class="container" style="margin-top:20px;">
    <h2>Your Ordered</h2>
        <table class="table border">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Order code</th>
                    <th>Order date</th>
                    <th>Order price</th>
                    <th>Order Status</th>
                    <th>Other</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orderall as $item)
                <tr data-orderid="{{$item->order_id}}">
                    <td>{{$loop->iteration}}</td>
                    <td class="code">{{$item->order_code}}</td>
                    <td>{{$item->order_date}}</td>
                    <td>{{$item->order_price}}</td>
                    <td class="status">{{$item->order_status}}</td>
                    <td class="buttoncontrol">
                        @if($item->order_status == "รอชำระเงิน")
                            <button class="btn btn-primary btn btn-sm upload">Upload</button>
                            <button class="btn btn-danger btn btn-sm cancle">Cancle</button>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
</div>

<script>
    $(function (){
        $(".upload").click(function (event){
            event.stopPropagation();
            var parent = $(this).parents('tr');
            var id = parent.data('orderid');
            var code = parent.find('.code').html();
           
            $.get('/upload_ref',{ id : id , code : code }, function (response){
                server_send();
                swal({
                        title : "Success",
                        text : "ชำระเงินเรียบร้อย",
                        type : 'success'
                    })
                parent.find('.buttoncontrol').html('');
                parent.find('.status').html('ชำระเงินแล้ว');
            });
        })
    });
   
</script>
@endsection