@extends('user.layouts.layout')
@section('content')



<div class="container" style="margin-top:30px;">
    @foreach ($product as $row)
        <div class="box" data-id="{{$row->p_id}}">
            <figure>
                <img class="box_img" src="/picture/{{$row->p_img}}" alt="">
            </figure>
            <div class="box_body">
                <h6>
                    <p class="title">
                        {{$row->p_name_TH}}
                    </p>
                </h6>
                <label class="red">THB</label><span class="red price"> {{$row->p_price}}</span>
                <button class="box_link buy_btn" >สั่งซื้อ</button>
            </div>
        </div> 
    @endforeach
   
@endsection