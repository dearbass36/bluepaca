<link rel="stylesheet" href="{{asset('css/user/header.css')}}">
    <nav class="navbar">
            <ul class="nav">
                <li><a href="{{url('Mock')}}">หน้าหลัก</a></li>
                <li><a>เงื่อนไขการบริการ</a></li>
                <li><a>การชำระเงิน</a></li>
                <li><a>ติดต่อเรา</a></li>
                <li style="float:right!important;" id="member_system">
                    <a>
                        <i class="fas fa-user" ></i>&nbsp;ระบบสมาชิก
                        &nbsp;<i class="fas fa-angle-down"></i>
                    </a>
                    <ul class="dropdown" style="display:none;">
                        <li><a href="{{url('/user/orderpage')}}">My Order</a></li>
                        <li><a>Setting</a></li>
                        <li><a>Logout</a></li>
                    </ul>
                </li>
                @php
                    $count = count($od);
                @endphp
                <li style="float:right!important;"><a class="cart_btn"><i class="fa fa-shopping-bag"></i><span class="total">{{$count}}</span></a></li>
            </ul>
    </nav>

    <div class="m">
            <div class="m-content">
                <div class="m-header">
                    <h3>Your Cart</h3>
                </div>
                <div class="m-body">
                    <div class="block">
                        @php
                        $total = 0;
                    @endphp
                    @isset($od)
                        @foreach ($od as $row)
                        @php
                            $total += round($row->od_qty*$row->p_price);    
                        @endphp
                            <div class="block-data text-center" data-rowid="{{$row->p_id}}" data-update="{{$row->od_id}}">
                                <div class="row oneline">
                                    <div class="col-md-4 figure">
                                        <img src="/picture/{{$row->p_img}}">
                                    </div>
                                    <div class="col-md-4 p_name text-center">
                                        {{$row->p_name_TH}}
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <a href="#" class="btn btn-danger btn btn-sm" onclick="del(this)">X</a>
                                    </div>
                                </div>
                                <div class="row twoline">
                                        <div class="col-md-5 text-center">
                                                <input type="number" min="1" value="{{$row->od_qty}}" oninput="plus(this,{{$row->p_price}})" class="qty">
                                        </div>
                                        <div class="col-md-7 text-center">
                                            Total :   THB <span class="p_total">{{round($row->od_qty*$row->p_price)}}</span>
                                        </div>
                                </div>
                            </div>
                        @endforeach
                    @endisset
                    </div>
                </div>
                <div class="m-footer">
                        delivery : 50 THB.&ensp;&ensp;
                        subtotal : <span id="final_total">{{$total}}</span> THB.&ensp;&ensp;<br><br>
                        Coupons : 
                            <input type="text" id="coupons_txt" class="" style="width:45%;">&nbsp;<button class="btn btn-primary btn btn-sm" onclick="Get_couponse()">GET CODE</button>
                        <br><br>
                        total : <span id="lastfinal_total">{{round($total+50)}}</span> THB.&ensp;&ensp;<br><br>
                        <button class="btn btn-danger" id="check_BTN" style="width:100%;">Checkout</button>
                </div>
            </div>
        </div>
        
        <script>
            $(function(){
                $(".buy_btn").click(function (event){
                    event.stopPropagation();
                    buy_click(this);
                    
                });
                $("#check_BTN").click(function (event){
                    event.stopPropagation();
                    checkout_click(this);
                    server_send();
                });
                $(".cart_btn").click(function (event){
                    event.stopPropagation();
                    $('.m-content').slideToggle();
                });
                $("#member_system").click(function (event){
                    event.stopPropagation();
                    $(this).children("ul").toggle();
                });
            });
            function buy_click(el) {
                var parentDiv = $(el).parents(".box");
                var id = parentDiv.data().id;
                var ParentTr = $(".block-data")
                var checkbuy = 0 ;
                var ParentTD = "";
                var price = $(el).parent('.box_body').children('.price').html();
                console.log(price);
                ParentTr.each(function(index,el){
                    var rowid = $(el).data().rowid;
                    if(rowid == id){
                        checkbuy++;
                        ParentTD = $(el);
                    }
                });
                if(checkbuy > 0){
                    var inputnumber = ParentTD.find('input');
                    var val = parseInt(inputnumber.val())+1;
                    inputnumber.val(val);
                    plus(inputnumber,price)
                }
                else {
                        var param = {
                            qty : 1,
                            owner : "{{Auth::user()->id}}",
                            p_id : id
                        }
                        $.get('/addtocart',{param},function(response){
                            
                            var img = parentDiv.children('figure').children('img');
                            var name = parentDiv.find('.title').html();
                            var price = parentDiv.find('.price').html();
                            var txt =       `<div class="block-data" data-rowid=${id} data-update=${response.id}>
                                                <div class="row oneline">
                                                    <div class="col-md-4">
                                                        <img src="${img[0].src}" width="100%">
                                                    </div>
                                                    <div class="col-md-4 p_name text-center">
                                                        ${name}
                                                    </div>
                                                    <div class="col-md-4 text-right">
                                                        <a href="#" class="btn btn-danger btn btn-sm" onclick="del(this)">X</a>
                                                    </div>
                                                </div>
                                                <div class="row twoline">
                                                    <div class="col-md-5 text-center">
                                                            <input type="number" min="1" value="1" oninput="plus(this,${price})" class="qty">
                                                    </div>
                                                    <div class="col-md-7 text-center">
                                                        Total :   THB <span class="p_total">${price}</span>
                                                    </div>
                                                </div>
                                            </div>`;
                            $(".block").append(txt).fadeIn();
                            var oldtotal = $("#final_total").text();
                            var newtotal = parseInt(oldtotal) + parseInt(price);
                            $("#final_total").text(newtotal);
                            $("#lastfinal_total").text(newtotal+50);
                            var x = parseInt($(".total").html());
                            $(".total").html(x+1);
                        });
                }
            }
            function del(el){
                var ParentTR = $(el).parents('.block-data');
                var childP = ParentTR.find('.p_total').text();
                var oldtotal = $("#final_total").text();
                var newtotal = parseInt(oldtotal) - parseInt(childP);
                var od_id = ParentTR.data().update;
                $("#final_total").text(newtotal);
                $("#lastfinal_total").text(newtotal+50);
                $(el).parents('.block-data').remove(); 
                var x = parseInt($(".total").html());
                $(".total").html(x-1);
                $.get('/deletecart',{od_id : od_id},function(response){
                        if(response.success == 'done'){
                           
                        }
                    });
            }
            function plus(el,price){
                var value = $(el).val();
                var ParentTR = $(el).parents('.block-data');
                var selectTotal = ParentTR.find('.p_total');
                var output = parseInt(value) * parseInt(price)
                var oldtotal = $("#final_total").text();
                var diff = parseInt(oldtotal) - parseInt(selectTotal.text())
                var newtotal = diff + output;
                $("#final_total").text(newtotal);
                $("#lastfinal_total").text(newtotal+50);
                selectTotal.html(output);
                var param = {
                            qty : value,
                            od_id : ParentTR.data().update
                    }
                    $.get('/addtocart',{param},function(response){
                        console.log(response);
                    });
            }
            function checkout_click(el){
                var DivBlock = $('.block');
                var Childblock = DivBlock.children('.block-data');
                var Total_price = $("#lastfinal_total").html();
                var param = [];
                Childblock.each(function(index,el){
                   var p_id = $(el).data().rowid;
                   var od_id = $(el).data().update;
                   var qty = $(el).find('.qty').val();
                   var array = {
                        p_id:p_id,
                        od_id:od_id,
                        qty : qty
                    }
                   param.push(array);
                });
                $.get("/save_order",{param : param , totalprice : Total_price ,owner : "{{ Auth::user()->id }}"},function(response){
                    swal({
                        title : "Success",
                        text : "สั่งสินค้าสำเร็จ",
                        type : 'success'
                    })
                    $(".block").html('');
                    $("#final_total").html('0');
                    $("#lastfinal_total").text("50");
                    $(".total").html('0');
                });
            };
            function Get_couponse(){
                var code = $("#coupons_txt").val();
                $.get('/get_code',{code:code},function(response){
                        var Coupons = response[0];
                        console.log(Coupons)
                        var oldtotal = parseInt($("#final_total").html());
                        var newtotal = 0;
                        if(Coupons['cp_mode'] == 2){
                            var percen = (((oldtotal * Coupons['cp_discount'])/100)).toFixed(0);
                            newtotal = oldtotal - percen;
                        }
                        else {
                            newtotal = oldtotal - Coupons['cp_discount'];
                        }
                        $("#lastfinal_total").html(newtotal+50);
                });
            }
        </script>