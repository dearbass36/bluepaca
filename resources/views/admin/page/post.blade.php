<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Blue Paca</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="../assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link href="../assets/plugins/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet" />
	<link href="../assets/plugins/font-awesome/5.0/css/fontawesome-all.min.css" rel="stylesheet" />
	<link href="../assets/css/default/style.min.css" rel="stylesheet" />
	<link href="../assets/css/default/style-responsive.min.css" rel="stylesheet" />
	<link href="{{asset('lib/css/bootstrap-tokenfield.min.css')}}" rel="stylesheet" />
    <link href="../assets/plugins/flag-icon/css/flag-icon.css" rel="stylesheet" />
    <link href="{{asset('css/admin/post.css')}}" rel="stylesheet">
	<!-- ================== END BASE CSS STYLE ================== -->
		<!-- ================== BEGIN BASE JS ================== -->
		<script src="../assets/plugins/pace/pace.min.js"></script>
		<!-- ================== BEGIN BASE JS ================== -->
	<script src="../assets/plugins/jquery/jquery-3.2.1.min.js"></script>
	<script src="../assets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="../assets/plugins/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
	<script src="{{asset('lib/js/sweetalert2.all.min.js')}}"></script>
	<script src="{{asset('lib/js/bootstrap-tokenfield.min.js')}}"></script>
	<script src="{{asset('js/admin/layout.js')}}"></script>
	<!-- ================== END BASE JS ================== -->

</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
    @include('admin.layouts.header')
    <!-- Body -->
   
        <div class="col-md-6 offset-3 box-main">
            <div class="head-line">
                <h2>&ensp;<i class="fas fa-shopping-basket">&nbsp;</i>รายการสั่งซื้อ : #{{$notic[0]->order_code}}</h2>
            </div>
            <div class="well" style="box-shadow: 2px 2px 8px rgba(0, 0, 0, 0.5);">
                <div class="box-body">
                    <div class="main-order">
                            <figure class="order-img">
                                <img src="/picture/{{$notic[0]->order_img}}">
                            </figure>
                            <div class="detail-box">
                                <h4>ผู้สั่ง : {{$notic[0]->name}}</h4>
                                <h4>ยอดที่ต้องชำระ : {{$notic[0]->order_price}} บาท</h4>
                                <h4>สถานะ : {{$notic[0]->order_status}}</h4>
                                <h4>วันที่สั่ง : {{$notic[0]->order_date}}</h4>
                            </div>
                    </div>
                    <div class="sub-product">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="width-100">สินค้า</th>
                                    <th>ชื่อสินค้า</th>
                                    <th class="width-100">จำนวน</th>
                                    <th class="width-100">ราคา</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $total = 0;
                                @endphp
                        @foreach ($detail as $row)
                                <tr>
                                    <td><figure class="product-img"><img src="/picture/{{$row->p_img}}"></figure></td>
                                    <td>{{$row->p_name_TH}}</td>
                                    <td>{{$row->od_qty}}</td>
                                    <td>
                                        @php
                                            $price = round($row->p_price * $row->od_qty);
                                            $total += $price;
                                        @endphp
                                        {{$price}}
                                    </td>
                                </tr>
                        @endforeach
                                <tr class="no-td">
                                    <td></td>
                                    <td></td>
                                    <td>ค่าจัดส่ง : </td>
                                    <td>
                                        @php 
                                        $deli = 0;
                                            if($total > 300) {
                                                echo "Free";
                                            }
                                            else {
                                                echo "50";
                                                $deli = 50 ;
                                            }
                                        @endphp
                                     </td>
                                </tr>
                                @php
                                    $diff = $notic[0]->order_price - $total - $deli;
                                @endphp
                                <tr class="no-td">
                                    <td></td>
                                    <td></td>
                                    <td>ส่วนลด : </td>
                                    <td>{{$diff}}</td> 
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    <!-- End Body -->
    <script src="../assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/plugins/js-cookie/js.cookie.js"></script>
    <script src="../assets/js/theme/default.min.js"></script>
    <script src="../assets/js/apps.min.js"></script>
    <!-- ================== END BASE JS ================== -->
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
</body>
</html