@extends('admin.layouts.layout')
@section('content')

		<!-- begin #content -->
		<div id="content" class="content">
                
                <!-- begin page-header -->
                <h1 class="page-header">หน้าแรก</h1>
                <!-- end page-header -->
                <!-- begin row -->
			<div class="row">
                    <!-- begin col-3 -->
                    <div class="col-lg-3 col-md-6">
                        <div class="widget widget-stats bg-red">
                            <div class="stats-icon"><i class="fa fa-shopping-bag"></i></div>
                            <div class="stats-info">
                                <h4>ORDERING</h4>
                                <p id="order_show">{{$countorder}}</p>	
                            </div>
                        </div>
                    </div>
                    <!-- end col-3 -->
                    <!-- begin col-3 -->
                    <div class="col-lg-3 col-md-6">
                        <div class="widget widget-stats bg-orange">
                            <div class="stats-icon"><i class="fa fa-money-bill-alt"></i></div>
                            <div class="stats-info">
                                <h4>PURCHASE ORDER</h4>
                                <p id="purchase_show">{{$orderAVG}}%</p>	
                            </div>
                        </div>
                    </div>
                    <!-- end col-3 -->
                    <!-- begin col-3 -->
                    <div class="col-lg-3 col-md-6">
                        <div class="widget widget-stats bg-black-lighter">
                            <div class="stats-icon"><i class="fa fa-flask"></i></div>
                            <div class="stats-info">
                                <h4>ALL PRODUCT</h4>
                                <p id="product_show">{{$countproduct}}</p>	
                            </div>
                        </div>
                    </div>
                    <!-- end col-3 -->
                     <!-- begin col-3 -->
                     <div class="col-lg-3 col-md-6">
                            <div class="widget widget-stats bg-grey-darker">
                                <div class="stats-icon"><i class="fa fa-users"></i></div>
                                <div class="stats-info">
                                    <h4>TOTAL VISITORS</h4>
                                    <p id="visitors_show">1,291,922</p>	
                                </div>
                            </div>
                        </div>
                        <!-- end col-3 -->      
                </div>
                <!-- end row -->
                <!-- begin row -->
			<div class="row">
                    <!-- begin col-12 -->
                    <div class="col-lg-12">
                        <div class="panel panel-inverse">
                            <!-- begin panel-heading -->
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                </div>
                                <h2 class="panel-title">PAYMENT</h2>
                            </div>
                            <!-- end panel-heading -->
                            <!-- begin panel-body -->
                            <div class="panel-body">
                                    Body
                            </div>
                            <!-- end panel-body -->
                        </div>
                    </div>
                    <!-- end col-12 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end #content -->
     
@endsection