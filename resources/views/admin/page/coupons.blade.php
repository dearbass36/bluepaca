@extends('admin.layouts.layout')
@section('content')
<link href="{{asset('css/admin/coupons.css')}}" rel="stylesheet">
<script src="../assets/plugins/ckeditor/ckeditor.js"></script> 
<!-- begin #content -->
	<!-- begin #content -->
                <div id="content" class="content">
                        <h1 class="page-header">คูปอง</h1>
                        <!-- begin row -->
			        <div class="row">
                                        <!-- begin col-12 -->
                                        <div class="col-lg-10 offset-1">
                                                <div class="panel panel-inverse">
                                                        <!-- begin panel-heading -->
                                                                <div class="panel-heading">
                                                                        <div class="panel-heading-btn">
                                                                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                                                        </div>
                                                                <h2 class="panel-title">PAYMENT</h2>
                                                                </div>
                                                        <!-- end panel-heading -->
                                                <!-- begin panel-body -->
                                                <div class="panel-body">
                                                        <div id="components-1">
                                                                <Blogging v-for="post in posts"
                                                                v-bind:key="post.id"
                                                                v-bind:title="post.title"></Blogging>
                                                        </div>
                                                        <div id="components-2">
                                                                        <Blogging v-for="post in posts"
                                                                        v-bind:key="post.id"
                                                                        v-bind:title="post.title"></Blogging>      
                                                        </div>
                                                </div>
                                                <!-- end panel-body -->
                                                </div>
                                        </div>
                                        <!-- end col-12 -->
                                </div>
                        <!-- end row -->
                </div>
<!-- end #content -->
        <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="{{asset('js/admin/coupons.js')}}"></script>
@endsection