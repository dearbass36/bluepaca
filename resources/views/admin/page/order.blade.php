@extends('admin.layouts.layout')
@section('content')
<link href="../assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="../assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="{{asset('css/admin/order.css')}}" rel="stylesheet" />

		<!-- begin #content -->
		<div id="content" class="content">
                
                <!-- begin page-header -->
                <h1 class="page-header">จัดการคำสั่งซื้อ</h1>
                <!-- end page-header -->
                
                <!-- begin panel -->
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        </div>
                        <h4 class="panel-title">Main Panel</h4>
                    </div>
                    <div class="panel-body">
                        <table id="data-table-default" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th width="1%"></th>
                                    <th width="1%" data-orderable="false">Ref.</th>
                                    <th class="text-nowrap">Order Code</th>
                                    <th class="text-nowrap">Owner</th>
                                    <th class="text-nowrap">Total Price</th>
                                    <th class="text-nowrap">Order Date</th>
                                    <th class="text-nowrap">Order Status</th>
                                </tr>
                            </thead>
                            <tbody id="t_body">
                                @foreach($order as $row)
                                    <tr class="odd gradeX" data-id="{{$row->order_id}}">
                                        <td width="1%" class="f-s-600 text-inverse">{{$loop->iteration}}.</td>
                                        <td width="1%" class="with-img"><a onclick="imgbtn_click({{$row->order_id}})"><img src="/picture/{{$row->order_img}}" class="img-rounded height-80" /></a></td>
                                        <td><a onclick="code_click({{$row->order_id}})">{{$row->order_code}}</a></td>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->order_price}}</td>
                                        <td>{{$row->order_date}}</td>
                                        <td>{{$row->order_status}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                       
                    </div>
                </div>
                <!-- end panel -->
            </div>
            <!-- end #content -->

            <div class="modal-img">
                <div class="mo-img-content">
                        <button type="button" class="close" >&times;</button>
                    <div class="mo-img-body">
                        <img class="img-mo" src="#"/>
                    </div>
                </div class="mo-img-body">
            </div>
    <script src="{{asset('js/admin/order.js')}}"></script>
                     <!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="../assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="../assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="../assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="../assets/js/demo/table-manage-default.demo.min.js"></script>
    <!-- ================== END PAGE LEVEL JS ================== -->
    <script>
            $(document).ready(function() {
                TableManageDefault.init();
            });
        </script> 
@endsection